# SKL Booking & Grants Web API


The SKL Web API enables for applications to query and handle available facilities, bookings, grants and organizational information.
It's based on simple REST principles and the [JSON API Spec](http://jsonapi.org).

[Swagger](https://swagger.io/) tools can be used to visualize and edit API Definitions. Browse to version [Latest](http://petstore.swagger.io?url=https://bitbucket.org/skl-bob/api/raw/master/specifications/oai/api.yaml), [Milestone 1](http://petstore.swagger.io?url=https://bitbucket.org/skl-bob/api/raw/m1/specifications/oai/api.yaml) or [PoC](http://petstore.swagger.io?url=https://bitbucket.org/skl-bob/api/raw/poc/specifications/oai/poc.yaml).

## Purpose

The main purpose with this repository is to develop the API Specification and keep track of issues. If you've found a bug or have ideas about improvements please [create an issue](https://bitbucket.org/skl-bob/api/issues/new) (access required).
 
## API Definition (OAI)

The Web API is described according to the [Open API Initiative](https://www.openapis.org) standard.
 
## License

![cc-logo](https://i.creativecommons.org/l/by/4.0/88x31.png)

This work is licensed under a [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).





 
 
