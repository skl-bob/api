#!/bin/bash

url="http://127.0.0.1:5959"

for f in $*; do
	case "$f" in
		*gathering*)
			path="gatherings"
			;;
		*customer*)
			path="customers"
			;;
		*resource*)
			path="resources"
			;;
		*group*)
			path="groups"
			;;
		*)
			path=""
			;;
	esac		
	echo
	echo "$f ..."
	curl -H "Content-Type: application/json" -d @"$f" $url/$path
done
